������ ������� �� ����������� uIP (���� TCP-IP), uMQTT (MQTT-������ v3.1 ��� uIP). ������� enc28j60 �� Jonathan Granade, ������������ Vladimir S. Fonov.

� net_config.h:
	������� ���������:
	MAC-����� ����: ��������� ETHADDR0, ETHADDR1, ETHADDR2, ETHADDR3, ETHADDR4, ETHADDR5
	���������/���������� DHCP-������� (��� ��������� ������������� IP-������ � ���������� ����): ��������� ENABLE_DHCP 1/0
	IP-�����, ����� �������, ���� ��-���������: ��������� IPADDR0, IPADDR1, IPADDR2, IPADDR3, NETMASK0, NETMASK1, NETMASK2, NETMASK3, DRIPADDR0, DRIPADDR1, DRIPADDR2, DRIPADDR3

� main.c:
	��������� ����������� � MQTT-�������:
	ID �������: ��������� MQTT_CLIENT_ID
	����� ��� ��������: ��������� MQTT_TOPIC_ALL
	IP-����� �������: ��������� MQTT_IP0, MQTT_IP1, MQTT_IP2, MQTT_IP3
